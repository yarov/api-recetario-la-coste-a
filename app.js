
/**
 * Module dependencies.
 */

 var express = require('express')
 , jsdom = require('jsdom')
 , request = require('request')
 , url = require('url')
 , _ = require("underscore")
 , cheerio= require("cheerio")
 , routes = require('./routes')
 , http = require('http')
 , path = require('path');

 var app = express();

 app.configure(function(){
    app.set('port', process.env.PORT || 5000);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(express.favicon());
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(app.router);
    app.use(require('less-middleware')({ src: __dirname + '/public' }));
    app.use(express.static(path.join(__dirname, 'public')));
 });

app.configure('development', function(){
    app.use(express.errorHandler());
    app.locals.pretty = true;
 });


 app.get('/', routes.index);
 app.get('/partials/:name', routes.partials);

 var sucursales = [];

 app.get ('/api/elglobo', function (req, res){
    request('http://www.elglobo.com.mx/nuestras-sucursales/', function (err, response, body){
     if (!err) {
       $ = cheerio.load(body) 
       $('#menumapa').find('li').length;
       $('#menumapa').find('li').each(function (i , item ){
         $estado = $(item).text();
         sucursales.push({name:$estado, url: $(item).find('a').attr('href')});
      });      
    };
 });
    res.send(sucursales);
 });



 app.post('/api/elglobo/sucursales', function (req, res){
   var url = req.body.url;
   var sucursales=[];
   var itemSucursal;
   request('http://www.elglobo.com.mx/'+ url, function (err, response, body){
      $ = cheerio.load(String(body));
      // var direccion = [];
      console.log($('option')[1].attribs.value);

      if ($('option')[1].attribs.value != undefined) {

         $('option').each(function (i, item){

            itemSucursal = item.attribs.value;
            // // if value is item 
            itemSucursal = itemSucursal.split('|');
            if (itemSucursal != '') {
               // console.log(itemSucursal);
               var sucursal ={ 
                  titulo : itemSucursal[0],
                  direccion : itemSucursal[1],
                  cp : itemSucursal[2],
                  municipio : itemSucursal[3],
                  telefono : itemSucursal[4],
                  latitud : itemSucursal[5],
                  longitud : itemSucursal[6]
                  // img : itemSucursal[7]
               }
               sucursales.push(sucursal)
               console.log(sucursal);
            };
         });

      }else{
          $('option').each(function (i, item){
            // if el paramtro no es definido 
            if (item.children[0].next != null) {
               
               itemSucursal = item.children[0].next.data.split('|');
               
               var tituloSucursal = itemSucursal[6].split('>');
               
               // console.log(itemSucursal);
               var sucursal ={ 
                  titulo :tituloSucursal[1],
               //    // direccion : itemSucursal[1],
                  cp : itemSucursal[1],
               //    // municipio : itemSucursal[3],
                  telefono : itemSucursal[3],
                  latitud : itemSucursal[4],
                  longitud : itemSucursal[5],
               //    // img : itemSucursal[7]
               }
               sucursales.push(sucursal)
               console.log(sucursal);
            };

          });
          

      };

      // res.json(sucursales);
   });
});




app.get('/api', function (req, res){
  // console.log("entro al api");
  var categorias = [];
  var recetas = [];
  request('http://www.lacostena.com.mx/recetario.html', function (error, response, body) {
     if (!error && response.statusCode == 200) {
      $ = cheerio.load(body);
      _.each($('#list-mas-productos li a'), function (item){
        url = $(item).attr('href').split('.html');
        url = url[0].split('/');
       // console.log();
       categorias.push({name:$(item).text(), url:url[3]});
    });
      res.json(categorias);
   }
});
});

app.get('/api/:url', function (req, res){
 var url = req.params.url;
 var ingredientes=[];
 var receta = [];
  // console.log('http://www.lacostena.com.mx/'+url+'.html');
  request('http://www.lacostena.com.mx/'+url+'.html', function (error, response, body){
     if (!error && response.statusCode == 200) {
      $ = cheerio.load(body);
      $('div.slide').each(function(i, item){

       var title = $(item).find('#wrapper-receta-izquierda h1').text(),
       $preparacion = $(item).find('#wrapper-receta-izquierda #wrapper-receta-preparacion p').text(),
       $platillo = $(item).find('#wrapper-receta-izquierda #wrapper-receta-preparacion img').attr('src');
       $producto = $(item).find('#wrapper-receta-derecha img').attr('src');


       $(item).find('#wrapper-receta-derecha table tbody tr').each(function(i, item){
        if ($(item).find('td:first-child').text() == "" && $(item).find('td:last-child').text() =="") {return false};
        var $cantidad = $(item).find('td:first-child').text()
        $descripccion = $(item).find('td:last-child').text()
          // console.log($(item).length);
          ingredientes.push({cantidad: $cantidad,descripcion:$descripccion});
          var listaIngredientes =  ingredientes;
       });
       receta.push({
        name:title, 
        preparacion : $preparacion, 
        images:{
         platillo:'http://www.lacostena.com.mx/'+$platillo,
         producto: 'http://www.lacostena.com.mx/'+$producto
      },
      listadoIngredientes : ingredientes
   });
       ingredientes = [];
    })
res.json(receta);      
}
});
});



app.listen(app.get('port'), function() {
 console.log("Listening on " + app.get('port'));
});

// http.createServer(app).listen(app.get('port'), function(){
//   console.log("Express server listening on port " + app.get('port'));
// });
