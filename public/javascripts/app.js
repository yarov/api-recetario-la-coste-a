/**
 * Api la costeña y el globo// servicio para mobile Module
 *
 * Description
 */
angular.module('apiMobile', []).
	config(['$routeProvider', function($routeProvider){
		$routeProvider.
		when('/', {templateUrl:'partials/index', controller: AppMobileApi}).
		when('/lacosteya', {templateUrl:'partials/lacosteya', controller: lacosteya}).
		when('/elglobo', {templateUrl:'partials/elglobo', controller: elglobo}).
		otherwise({redirecTo:'/'})


	}]);